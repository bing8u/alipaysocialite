<?php

namespace Sbing\AliPaySocialite;

use SocialiteProviders\Manager\SocialiteWasCalled;

class AliPayExtendSocialite
{
    public function handle(SocialiteWasCalled $socialiteWasCalled)
    {
        $socialiteWasCalled->extendSocialite('alipay', __NAMESPACE__.'\Provider');
    }
}
