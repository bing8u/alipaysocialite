# Socialite 支付宝授权登录

#### 介绍
基于Socialite Providers 进行的支付宝授权登录

#### 安装教程

```php
$ composer require sbing/alipaysocialite
```

#### 配置 config

config/services.php 

```php
'alipay' => [
    'client_id' => env('ALIPAY_KEY'),
    'client_secret' => env('ALIPAY_SECRET'),
    'redirect' => env('ALIPAY_REDIRECT_URI'),
],
```

#### 配置 Provider

app/Providers/EventServiceProvider.php

```php
use Sbing\AliPaySocialite\AliPayExtendSocialite;
use SocialiteProviders\Manager\SocialiteWasCalled;


protected $listen = [
    SocialiteWasCalled::class => [
        AliPayExtendSocialite::class,
    ],
];
```

参考
- Laravel Socialite [详细文档](https://learnku.com/docs/laravel/7.x/socialite/7517)
- Socialite Providers [详细文档](https://socialiteproviders.netlify.app/)
